using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapaMultitouch : MonoBehaviour
{
    private Vector3 startPosition;
    void  Start()
    {
        startPosition = transform.position;
        //Debug.Log(startPosition);
    }
    public GameObject Mapa,mapa1,canvas2;
    public void ActivarMapa()
    {
        canvas2.SetActive(false);
        Mapa.SetActive(true);
        mapa1.SetActive(true);
    }
    public void DesactivarMapa()
    {
        Mapa.SetActive(false);
        mapa1.SetActive(false);
        canvas2.SetActive(true);
        Camera.main.orthographicSize=5;
        transform.position = startPosition;
        //Camera.Reset();
        //Debug.Log(startPosition);
        //Debug.Log(transform.position);
    }
}
