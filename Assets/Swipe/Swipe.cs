using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swipe : MonoBehaviour
{
    public player Player;
    private Vector2 PosicionInicial;
    public float SwipeMinY; //0.5
    public float SwipeMinX; //0.5

    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                PosicionInicial = touch.position;
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                float swipeVertical = (new Vector3(0, touch.position.y, 0) - new Vector3(0, PosicionInicial.y, 0)).magnitude;
                float swipeHorizontal=(new Vector3(0, touch.position.x, 0) - new Vector3(0, PosicionInicial.x, 0)).magnitude;
                if (swipeVertical > SwipeMinY && swipeVertical>swipeHorizontal)
                {
                    float u = Mathf.Sign(touch.position.y -PosicionInicial.y);
                    if (u > 0)
                    {
                        Player.Jump1();
                        print("Arriba");//Arriba
                    }
                    if (u < 0)
                    {
                        print("Abajo");
                    }
                }
                if (swipeHorizontal > SwipeMinX && swipeHorizontal>swipeVertical)
                {
                    float u = Mathf.Sign(touch.position.x - PosicionInicial.x);
                    if (u > 0)
                    {
                       Player.AttackD();
                       print("Derecha");
                       //Derecha
                    }
                    if (u < 0)
                    {
                       Player.AttackA();
                       print("Izquierda");
                       //Izquierda
                    }
                }
            }
            /*float swipeVertical2 = (new Vector3(0, touch.position.y, 0) - new Vector3(0, PosicionInicial.y, 0)).magnitude;
            float swipeHorizontal = (new Vector3(touch.position.x, 0, 0) - new Vector3(PosicionInicial.x, 0, 0)).magnitude;
            {
                float u = Mathf.Sign(touch.position.x - PosicionInicial.x);
                if (u > 0)
                {
                    Player.AttackD();
                    print("Derecha");
                    //Derecha
                }
                if (u < 0)
                {
                    Player.AttackA();
                    print("Izquierda");
                    //Izquierda
                }
            }*/
        }
    }
}
