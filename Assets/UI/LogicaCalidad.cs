using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LogicaCalidad : MonoBehaviour
{
    public TMP_Dropdown dropdown;
    public int calidad;
    // Start is called before the first frame update
    void Start()
    {
        calidad = PlayerPrefs.GetInt("numeroDecalidad", 3);
        dropdown.value = calidad;
        ajustarCalidad();
    }
    public void ajustarCalidad()
    {
        QualitySettings.SetQualityLevel(dropdown.value);
        PlayerPrefs.SetInt("numeroDecalidad", dropdown.value);
        calidad = dropdown.value;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
