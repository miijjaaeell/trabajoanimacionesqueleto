using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;

public class coins : MonoBehaviour
{
    public int coin;
    public Text txtC;
    Camera Camera;
    // Update is called once per frame
    void Start()
    {
        Camera=Camera.main;
        txtC.text=GameManager.instance.coins.ToString();
    }
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.K))
        {
            coin+=1;
            GameManager.instance.coins++;
            txtC.text=GameManager.instance.coins.ToString();
        }
        if(Input.touchCount> 0)
        {
            Touch miToque=Input.GetTouch(0);
            Vector2 test = Camera.ScreenToWorldPoint(miToque.position);
            RaycastHit2D hit = Physics2D.Raycast(test,Vector2.zero);
            if(hit.collider.tag=="coin")
            {
                coin+=1;
                GameManager.instance.coins++;
                txtC.text="x"+GameManager.instance.coins.ToString();
                Object.Destroy(hit.transform.gameObject);
                //Destroy(gameObject);
            }
        }
        /*if(Input.GetMouseButtonDown (0))
        {
            Vector2 test = Camera.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(test,Vector2.zero);
            if(hit.collider.tag=="coin")
            {
                coin+=1;
                GameManager.instance.coins++;
                txtC.text="x"+GameManager.instance.coins.ToString();
                Object.Destroy(hit.transform.gameObject);
                //Destroy(gameObject);
            }
        }*/
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            coin+=1;
            GameManager.instance.coins++;
            txtC.text="x"+GameManager.instance.coins.ToString();
            Destroy(gameObject);
        }   
    }
}
