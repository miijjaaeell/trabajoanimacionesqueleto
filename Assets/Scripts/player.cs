using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class player : MonoBehaviour
{
    public static player instance;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public float Jumpforce, speed,maxSpeed;
    public float hielo;
    private Rigidbody2D rb;
    private float Horizontal;
    private bool movement = true;
    private bool Ground;
    private Animator Animator;
    public GameObject GameOver;
    Vector2 dirVector;
    private Vector2 PosicionInicial;
    public float SwipeMinY; //0.5
    public float SwipeMinX; //0.5

    // Start is called before the first frame update
    void Start()
    {
        Animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!movement) Horizontal = 0;
        if (Horizontal < 0.0f) transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
        else if (Horizontal > 0.0f) transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);

        //Animator.SetBool("Runing", Horizontal != 0.0f);

        Debug.DrawRay(transform.position, Vector3.down * 2.1f, Color.blue);
        if (Physics2D.Raycast(transform.position, Vector3.down, 2.1f))
        {
            Ground = true;
            Animator.SetBool("jump2", false);
            Animator.SetBool("jump", false);

        }
        else
        {
            Animator.SetBool("jump2", true);
            Animator.SetBool("jump", true);
            Ground = false;
        }

        if (Input.GetKeyDown(KeyCode.D) )
        {
            Animator.SetBool("Runing", true);
            transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
        }
        if (Input.GetKeyDown(KeyCode.A) )
        {
            Animator.SetBool("Runing", true);
            transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
        }
        if (Input.GetKeyUp(KeyCode.D) && Ground)
        {
            Animator.SetBool("Runing", false);
        }
        if (Input.GetKeyUp(KeyCode.A) && Ground)
        {
            Animator.SetBool("Runing", false);
        }
    }
    public void Jump(InputAction.CallbackContext context)
    {
        if (context.performed && Ground)
        {
            rb.AddForce(Vector2.up * Jumpforce, ForceMode2D.Impulse);
        }
    }
    public void Jump1()
    {
        if(Ground)
        {
          rb.AddForce(Vector2.up * Jumpforce, ForceMode2D.Impulse);
        }
    }
    public void AttackD()
    {
        Animator.SetBool("attack1", true);
        transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
        Invoke("attackAN",0.2f);
    }
    public void attackAN()
    {
        Animator.SetBool("attack1", false);
    }
    public void AttackA()
    {
        Animator.SetBool("attack1", true);
        transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
        Invoke("attackAN",0.2f);
    }

    public void Attack(InputAction.CallbackContext context)
    {
        if (context.performed && Ground)
        {
            Animator.SetBool("attack1", true);
        }
        else
        {
            Animator.SetBool("attack1", false);
        }
    }
    public void Move(InputAction.CallbackContext context)
    {
        dirVector = context.ReadValue<Vector2>();
    }
    private void FixedUpdate()
    {
        rb.velocity = new Vector2(dirVector.x * speed, rb.velocity.y);
    }
}
