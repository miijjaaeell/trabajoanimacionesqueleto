using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PausaMenu : MonoBehaviour
{
    public static bool GameIsPaused = false;
    public GameObject PaseMenuUI, ELEperosnaje,player1,player2;
    // Update is called once per frame
    void Start()
    {
        PlayerPrefs.GetInt("skinp");
        if(PlayerPrefs.GetInt("skinp")==1)
        {
            player1.SetActive(true);
        }
        if(PlayerPrefs.GetInt("skinp")==2)
        {
            player2.SetActive(true);
        }
       
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }
    public void Resume()
    {
        PaseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
    }
    void Pause()
    {
        PaseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }
    public void loadMenu()
    {
        SceneManager.LoadScene("Menu");
        PlayerPrefs.DeleteAll();
    }
    public void Pers1()
    {
        PlayerPrefs.SetInt("skinp",1);
        ELEperosnaje.SetActive(false);
        player1.SetActive(true);
    }
    public void Pers2()
    {
        PlayerPrefs.SetInt("skinp",2);
        ELEperosnaje.SetActive(false);
        player2.SetActive(true);
    }

}
