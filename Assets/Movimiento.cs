using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Movimiento : MonoBehaviour
{
    public float Jumpforce, speed, maxSpeed;
    public float hielo;
    private Rigidbody2D Rigidbody2D;
    private float Horizontal;
    private bool movement = true;
    private bool Ground;
    private Animator Animator;
    public GameObject GameOver;
    Vector2 dirVector;
    // Start is called before the first frame update
    void Start()
    {
        Animator = GetComponent<Animator>();
        Rigidbody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!movement) Horizontal = 0;
        if (Horizontal < 0.0f) transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
        else if (Horizontal > 0.0f) transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);

        Animator.SetBool("Runing", Horizontal != 0.0f);

        Debug.DrawRay(transform.position, Vector3.down * 2.1f, Color.blue);
        if (Physics2D.Raycast(transform.position, Vector3.down, 2.1f))
        {
            Ground = true;
            Animator.SetBool("jump2", false);
            Animator.SetBool("jump", false);

        }
        else
        {
            Animator.SetBool("jump2", true);
            Animator.SetBool("jump", true);
            Ground = false;
        }
        if (Input.GetKeyDown(KeyCode.W) && Ground)
        {
            Jump();
        }
    }
    //public void Move(InputAction.CallbackContext context)
    //{
    //    dirVector = context.ReadValue<Vector2>();
    //}
    void Jump()
    {
        Rigidbody2D.AddForce(Vector2.up * Jumpforce, ForceMode2D.Impulse);
    }
    private void FixedUpdate()
    {
        //Rigidbody2D.velocity = new Vector2(dirVector.x * speed, dirVector.y * speed);
    }
}
